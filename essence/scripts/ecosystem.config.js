module.exports = {
  apps: [
    {
      name: 'essense',
      cwd: '.',
      script: 'npm run start:prod',
      instances: 'max',
      autorestart: true,
      watch: false,
      max_memory_restart: '2G',
    },
  ],
};
