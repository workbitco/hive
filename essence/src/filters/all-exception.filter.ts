import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import * as _ from 'lodash';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const status =
      exception instanceof HttpException ? exception.getStatus() : 500;

    const message = _.get(exception, 'message');
    let messages: string[];

    if (_.has(message, 'message')) {
      messages = this.extractError(_.get(message, 'message'));
    } else {
      messages = this.extractError(message);
    }

    if (!messages || messages.length === 0) {
      messages = ['Internal Server Error'];
    }

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url,
      messages,
    });
  }

  private extractError(message: any): string[] {
    let messages: string[];

    if (_.isArray(message)) {
      messages = message.reduce((list: string[], el) => {
        const errors = _.get(el, 'constraints');
        if (errors) {
          return [...list, ...(Object.values(errors) as string[])];
        }
        return list;
      }, []);
    } else if (_.isObject(message)) {
      const error =
        _.get(message, 'message') ||
        _.get(message, 'error') ||
        'Internal Server Error';
      messages = [error];
    } else if (_.isString(message)) {
      messages = [message];
    }

    return messages;
  }
}
