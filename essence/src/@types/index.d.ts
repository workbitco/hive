type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

import { Document } from 'mongoose';

interface BaseDocument extends Document {
  readonly sid: string;
  createdAt: Date;
  updatedAt: Date;
}

interface FileBuffer {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  buffer: Buffer;
  size: number;
}
