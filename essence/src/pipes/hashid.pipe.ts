import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import * as _ from 'lodash';
import { hashids } from 'schemas/schema.plugin';

@Injectable()
export class HashIdPipe implements PipeTransform {
  constructor(private readonly encodedKeys: string[]) {}

  transform(value: any, metadata: ArgumentMetadata) {
    if (metadata.type === 'param' && metadata.data === 'id') {
      return hashids.decodeHex(value);
    }
    return Object.keys(value).reduce((aggr, key) => {
      if (this.encodedKeys.includes(key)) {
        return {
          ...aggr,
          [key]: hashids.decodeHex(value[key]),
        };
      }
      return aggr;
    }, value);
  }
}
