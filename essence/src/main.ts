import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import { AllExceptionsFilter } from 'filters/all-exception.filter';
import * as helmet from 'helmet';
import { TimeoutInterceptor } from 'interceptors/timeout.interceptor';
import { TransformInterceptor } from 'interceptors/transform.interceptor';
import { AppModule } from 'modules/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {});
  app.use(cookieParser());
  app.enableCors();
  app.use(helmet());
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalInterceptors(
    new TransformInterceptor(),
    new TimeoutInterceptor(),
  );
  app.useGlobalFilters(new AllExceptionsFilter());

  const options = new DocumentBuilder()
    .setTitle('WorkBit')
    .setDescription('The WorkBit API specifications')
    .setVersion('1.0')
    .addBearerAuth('Authorization', 'header')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('specs', app, document);

  app.enableShutdownHooks();
  await app.listen(3001);
}

bootstrap();
