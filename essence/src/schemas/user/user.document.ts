import { BaseDocument } from '@types';

export interface UserDocument extends BaseDocument {
  readonly email: string;
  readonly password: string;
}
