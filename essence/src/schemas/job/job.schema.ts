import { Schema } from 'mongoose';
import { JobDocument } from 'schemas/job/job.document';
import { hashIdPlugin } from 'schemas/schema.plugin';

export const JobSchema = new Schema<JobDocument>({
  position: String,
  type: { type: String, enum: ['remote', 'local'], default: 'remote' },
  location: String,
  category: String,
  tags: [String],
  description: String,
  requirement: String,
  salary: Number,
  emails: [String],
  stylings: String,
  company: { type: Schema.Types.ObjectId, ref: 'Company' },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

JobSchema.plugin(hashIdPlugin);
