import { BaseDocument } from '@types';
import { CompanyDocument } from 'schemas/company/company.document';
import { UserDocument } from 'schemas/user/user.document';

export interface JobDocument extends BaseDocument {
  readonly position: string;
  readonly type: string;
  readonly location: string;
  readonly category: string;
  readonly tags: string[];
  readonly description: string;
  readonly requirement: string;
  readonly salary: number;
  readonly emails: string[];
  readonly stylings: string;
  readonly company: CompanyDocument | string;
  readonly user: UserDocument | string;
}
