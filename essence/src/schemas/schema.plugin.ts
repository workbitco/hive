import { BaseDocument } from '@types';
import { config } from 'config';
import Hashids from 'hashids';
import { Schema } from 'mongoose';

export const hashids = new Hashids(config.HASH_SECRET);

export function hashIdPlugin(schema: Schema) {
  // schema.pre<BaseDocument>('save', async function() {
  //   this.updatedAt = new Date();
  // });

  schema.pre<BaseDocument>('update', async function() {
    this.update({}, { $set: { updatedAt: new Date() } });
  });

  schema.virtual('sid').get(function() {
    if (this && this.id) {
      return hashids.encodeHex(this.id);
    }
  });
}
