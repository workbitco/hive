import { Schema } from 'mongoose';
import { hashIdPlugin } from 'schemas/schema.plugin';
import { UserDocument } from 'schemas/user/user.document';

export const UserSchema = new Schema<UserDocument>({
  email: { type: String, index: true, unique: true },
  password: String,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

UserSchema.plugin(hashIdPlugin);
