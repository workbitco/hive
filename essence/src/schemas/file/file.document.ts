import { BaseDocument } from '@types';

export interface FileDocument extends BaseDocument {
  readonly metadata: { [key: string]: any };
  readonly path: string;
}
