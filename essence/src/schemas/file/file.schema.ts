import { Schema } from 'mongoose';
import { FileDocument } from 'schemas/file/file.document';
import { hashIdPlugin } from 'schemas/schema.plugin';

export const FileSchema = new Schema<FileDocument>({
  metadata: { type: Map, of: Schema.Types.Mixed },
  path: String,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

FileSchema.plugin(hashIdPlugin);
