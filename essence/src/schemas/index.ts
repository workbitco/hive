import { ApplicationSchema } from 'schemas/application/application.schema';
import { CodeSchema } from 'schemas/code/code.schema';
import { CompanySchema } from 'schemas/company/company.schema';
import { FileSchema } from 'schemas/file/file.schema';
import { JobSchema } from 'schemas/job/job.schema';
import { UserSchema } from 'schemas/user/user.schema';

export const MongooseFeatures = {
  application: { name: 'Application', schema: ApplicationSchema },
  code: { name: 'Code', schema: CodeSchema },
  company: { name: 'Company', schema: CompanySchema },
  file: { name: 'File', schema: FileSchema },
  job: { name: 'Job', schema: JobSchema },
  user: { name: 'User', schema: UserSchema },
};
