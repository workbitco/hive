import { BaseDocument } from '@types';
import { FileDocument } from 'schemas/file/file.document';
import { JobDocument } from 'schemas/job/job.document';
import { UserDocument } from 'schemas/user/user.document';

export interface ApplicationDocument extends BaseDocument {
  readonly remark: string;
  readonly job: JobDocument | string;
  readonly attachment: FileDocument | string;
  readonly user: UserDocument | string;
}
