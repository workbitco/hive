import { Schema } from 'mongoose';
import { ApplicationDocument } from 'schemas/application/application.document';
import { hashIdPlugin } from 'schemas/schema.plugin';

export const ApplicationSchema = new Schema<ApplicationDocument>({
  remark: String,
  job: { type: Schema.Types.ObjectId, ref: 'Job' },
  attachment: { type: Schema.Types.ObjectId, ref: 'File' },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

ApplicationSchema.plugin(hashIdPlugin);
