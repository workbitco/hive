import { BaseDocument } from '@types';

export interface CodeDocument extends BaseDocument {
  readonly email: string;
  readonly secret: string;
}
