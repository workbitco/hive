import { Schema } from 'mongoose';
import * as nanoid from 'nanoid';
import { CodeDocument } from 'schemas/code/code.document';
import { hashIdPlugin } from 'schemas/schema.plugin';

export const CodeSchema = new Schema<CodeDocument>({
  email: { type: String, index: true, unique: true },
  secret: { type: String, default: nanoid(32) },
  createdAt: {
    type: Date,
    default: Date.now,
    index: { unique: false, expires: '1m' },
  },
  updatedAt: { type: Date, default: Date.now },
});

CodeSchema.plugin(hashIdPlugin);
