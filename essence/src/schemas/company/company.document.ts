import { BaseDocument } from '@types';
import { FileDocument } from 'schemas/file/file.document';
import { UserDocument } from 'schemas/user/user.document';

export interface CompanyDocument extends BaseDocument {
  readonly name: string;
  readonly url: string;
  readonly attachments: FileDocument[] | string[];
  readonly user: UserDocument | string;
}
