import { Schema } from 'mongoose';
import { CompanyDocument } from 'schemas/company/company.document';
import { hashIdPlugin } from 'schemas/schema.plugin';

export const CompanySchema = new Schema<CompanyDocument>({
  name: String,
  url: String,
  attachments: [{ type: Schema.Types.ObjectId, ref: 'File' }],
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

CompanySchema.plugin(hashIdPlugin);
