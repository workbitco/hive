import {
  BadRequestException,
  Controller,
  Get,
  HttpException,
  Inject,
  InternalServerErrorException,
  NotAcceptableException,
  Param,
  Post,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitFile,
  ApiProduces,
} from '@nestjs/swagger';
import { FileBuffer } from '@types';
import { Request, Response } from 'express';
import { JwtAuthGuard } from 'guards/jwt-auth.guard';
import * as _ from 'lodash';
import { StorageService } from 'modules/storage/storage.service';
import * as multer from 'multer';
import { HashIdPipe } from 'pipes/hashid.pipe';
import { throwError, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export const AcceptContentTypes = [
  'application/msword',
  'application/pdf',
  'image/jpeg',
  'image/jpg',
  'image/png',
];

@Controller('storage')
export class StorageController {
  @Inject()
  private readonly storageService: StorageService;

  private acceptedTypes = ['photo', 'video'];

  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true })
  @UseInterceptors(
    FileInterceptor('file', {
      limits: { fileSize: 15_360_000 },
      storage: multer.memoryStorage(),
      fileFilter(req, file, next) {
        const isAcceptType = AcceptContentTypes.indexOf(file.mimetype) > -1;
        if (isAcceptType) {
          next(null, true);
        } else {
          next(new NotAcceptableException('File not supported'), false);
        }
      },
    }),
  )
  @UseGuards(JwtAuthGuard)
  @Post('upload/:type')
  uploadFile(
    @UploadedFile() file: FileBuffer,
    @Param('type') type: string,
    @Req() req: Request,
  ): Observable<{ id: string }> {
    if (this.acceptedTypes.indexOf(type) === -1) {
      return throwError(new BadRequestException());
    }

    const userId = _.get(req, 'user._id');
    const path = `${type}/${userId}`;

    return this.storageService.upload(file, path).pipe(
      map(val => ({
        id: val.sid,
      })),
    );
  }

  @ApiProduces(...AcceptContentTypes)
  @UsePipes(new HashIdPipe(['id']))
  @Get(':id')
  downloadFile(@Param('id') id: string, @Res() res: Response) {
    this.storageService.download(id).subscribe(
      ({ metadata, stream }) => {
        res.set({
          'Content-Type': metadata.metaData['content-type'],
          'Content-Length': metadata.size,
        });

        stream.pipe(res);
      },
      error => {
        if (error instanceof HttpException) {
          res.status(error.getStatus()).send(error);
        } else {
          res.status(500).send(new InternalServerErrorException());
        }
      },
    );
  }
}
