import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { FileDocument } from 'schemas/file/file.document';

@Injectable()
export class StorageConnection {
  @InjectModel('File')
  private readonly filePvd: Model<FileDocument>;

  create({ metadata, path }: Partial<FileDocument>): Observable<FileDocument> {
    return from(this.filePvd.create({ metadata, path }));
  }

  findById({ id }: { id: string }): Observable<FileDocument> {
    return from(this.filePvd.findById(id));
  }
}
