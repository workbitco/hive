import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { FileBuffer } from '@types';
import { BucketItemStat, Client } from 'minio';
import { StorageConnection } from 'modules/storage/storage.connection';
import { from, of, throwError, zip, Observable } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';
import { FileDocument } from 'schemas/file/file.document';
import { Stream } from 'stream';

@Injectable()
export class StorageService {
  @Inject('MINIO')
  private readonly minioClient: Client;

  @Inject()
  private readonly storageConnection: StorageConnection;

  private readonly bucketName = 'workbit';

  getBucket(): Observable<boolean> {
    const getBucketPromise = async () => {
      try {
        const existed = await this.minioClient.bucketExists(this.bucketName);
        if (existed) return true;
        await this.minioClient.makeBucket(this.bucketName, 'sgp1');
        return true;
      } catch (error) {
        console.log(error);
        return false;
      }
    };

    return from(getBucketPromise());
  }

  upload(file: FileBuffer, path: string): Observable<FileDocument> {
    return this.getBucket().pipe(
      switchMap(existed => {
        if (!existed) return of(undefined);
        return this.storageConnection
          .create({
            metadata: {
              originalName: file.originalname,
              encoding: file.encoding,
              mimeType: file.mimetype,
              size: file.size,
            },
            path,
          })
          .pipe(
            mergeMap(fileDocument => {
              const objectName = `${path}/${fileDocument.id}`;
              return from(
                this.minioClient.putObject(
                  this.bucketName,
                  objectName,
                  file.buffer,
                  file.size,
                  {
                    'Content-Type': file.mimetype,
                    'X-Amz-Meta-Original-Name': file.originalname,
                    'X-Amz-Meta-Encoding': file.encoding,
                  },
                ),
              ).pipe(etag => of(fileDocument));
            }),
          );
      }),
    );
  }

  download(
    id: string,
  ): Observable<{ metadata: BucketItemStat; stream: Stream }> {
    return this.getBucket().pipe(
      switchMap(existed => {
        if (!existed) return of(undefined);
        return this.storageConnection
          .findById({ id })
          .pipe(
            switchMap(fileDocument => {
              if (!fileDocument) {
                return throwError(new NotFoundException());
              }
              const objectName = `${fileDocument.path}/${id}`;
              this.minioClient.presignedGetObject(this.bucketName, objectName);
              return zip(
                from(
                  this.minioClient.statObject(
                    this.bucketName,
                    `${fileDocument.path}/${id}`,
                  ),
                ),
                from(
                  this.minioClient.getObject(
                    this.bucketName,
                    `${fileDocument.path}/${id}`,
                  ),
                ),
              );
            }),
          )
          .pipe(map(([metadata, stream]) => ({ metadata, stream })));
      }),
    );
  }
}
