import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from 'modules/auth/auth.module';
import { StorageConnection } from 'modules/storage/storage.connection';
import { StorageController } from 'modules/storage/storage.controller';
import { StorageService } from 'modules/storage/storage.service';
import { MinIOProvider } from 'providers/storage.provider';
import { MongooseFeatures } from 'schemas';

@Module({
  imports: [AuthModule, MongooseModule.forFeature([MongooseFeatures.file])],
  controllers: [StorageController],
  providers: [MinIOProvider, StorageConnection, StorageService],
})
export class StorageModule {}
