import { MongooseModule } from '@nestjs/mongoose';
import { config } from 'config';

export const DatabaseModule = MongooseModule.forRootAsync({
  useFactory: () => ({
    uri: config.MONGODB_URL,
    useNewUrlParser: true,
    useFindAndModify: false,
  }),
});
