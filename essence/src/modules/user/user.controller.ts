import { Body, Controller, Inject, Post } from '@nestjs/common';
import { AuthService } from 'modules/auth/auth.service';
import {
  AuthUserDto,
  RecoverUserDto,
  ResetUserDto,
} from 'modules/user/dtos/user.dto';
import { UserService } from 'modules/user/user.service';
import { of, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Controller('user')
export class UserController {
  @Inject()
  private readonly authService: AuthService;

  @Inject()
  private readonly userService: UserService;

  @Post()
  authenticate(
    @Body() authUserDto: AuthUserDto,
  ): Observable<{ token: string }> {
    return this.userService.authenticate(authUserDto).pipe(
      mergeMap(val => {
        const token = this.authService.sign({
          id: val.sid,
          email: authUserDto.email,
        });
        return of({ token });
      }),
    );
  }

  @Post('recover')
  recover(
    @Body() recoverUserDto: RecoverUserDto,
  ): Observable<{ success: boolean }> {
    return this.userService.recover(recoverUserDto);
  }

  @Post('reset')
  reset(@Body() resetUserDto: ResetUserDto): Observable<{ token: string }> {
    return this.userService.reset(resetUserDto).pipe(
      mergeMap(val => {
        const token = this.authService.sign(
          {
            id: val.sid,
            email: val.email,
          },
          900 /* 15 minutes */,
        );
        return of({ token });
      }),
    );
  }
}
