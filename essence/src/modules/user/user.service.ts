import {
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import * as argon2 from 'argon2';
import * as _ from 'lodash';
import { EmailService } from 'modules/common/email.service';
import {
  AuthUserDto,
  RecoverUserDto,
  ResetUserDto,
} from 'modules/user/dtos/user.dto';
import { UserConnection } from 'modules/user/user.connection';
import { from, of, throwError, Observable } from 'rxjs';
import { map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { UserDocument } from 'schemas/user/user.document';

@Injectable()
export class UserService {
  @Inject()
  private readonly emailService: EmailService;

  @Inject()
  private readonly userConnection: UserConnection;

  authenticate({ email, password }: AuthUserDto): Observable<UserDocument> {
    return this.userConnection.findByEmail({ email }).pipe(
      switchMap(val => {
        if (_.isNil(val)) {
          return from(argon2.hash(password)).pipe(
            mergeMap(hash => {
              return this.userConnection.create({ email, password: hash });
            }),
          );
        }

        return from(argon2.verify(val.password, password)).pipe(
          switchMap(valid =>
            valid
              ? of(val)
              : throwError(new UnauthorizedException('NOT_MATCHED')),
          ),
        );
      }),
    );
  }

  recover({ email }: RecoverUserDto): Observable<{ success: boolean }> {
    return this.userConnection.findByEmail({ email }).pipe(
      switchMap(val => {
        if (_.isNil(val)) {
          return of({ success: true });
        }
        return this.userConnection
          .generateOTP({ email })
          .pipe(
            tap(() => {
              // this.emailService.sendEmail([email], '').subscribe();
            }),
          )
          .pipe(map(generated => ({ success: generated })));
      }),
    );
  }

  reset({ email, secret }: ResetUserDto): Observable<UserDocument> {
    return this.userConnection.verifyOTP({ email, secret }).pipe(
      mergeMap(valid => {
        if (!valid) {
          return throwError(new NotFoundException());
        }
        return this.userConnection.findByEmail({ email });
      }),
    );
  }
}
