import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from 'modules/auth/auth.module';
import { UserConnection } from 'modules/user/user.connection';
import { UserController } from 'modules/user/user.controller';
import { UserService } from 'modules/user/user.service';
import { MongooseFeatures } from 'schemas';

@Module({
  imports: [
    AuthModule,
    MongooseModule.forFeature([MongooseFeatures.code, MongooseFeatures.user]),
  ],
  controllers: [UserController],
  providers: [UserConnection, UserService],
})
export class UserModule {}
