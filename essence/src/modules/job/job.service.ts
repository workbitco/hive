import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import * as _ from 'lodash';
import { EmailService } from 'modules/common/email.service';
import { CreateApplicationDto } from 'modules/job/dtos/application.dto';
import { CreateJobDto } from 'modules/job/dtos/job.dto';
import { JobConnection } from 'modules/job/job.connection';
import { throwError, Observable } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';
import { ApplicationDocument } from 'schemas/application/application.document';
import { JobDocument } from 'schemas/job/job.document';

@Injectable()
export class JobService {
  @Inject()
  private readonly jobConnection: JobConnection;

  @Inject()
  private readonly emailService: EmailService;

  create(
    {
      position,
      type,
      location,
      category,
      tags,
      description,
      requirement,
      salary,
      emails,
      stylings,
      companyId,
    }: CreateJobDto,
    userId: string,
  ): Observable<JobDocument> {
    return this.jobConnection.create({
      position,
      type,
      location,
      category,
      tags,
      description,
      requirement,
      salary,
      emails,
      stylings,
      company: companyId,
      user: userId,
    });
  }

  findById(id: string): Observable<JobDocument> {
    return this.jobConnection.findById({ id });
  }

  findByMetadata({
    skip,
    limit,
    type,
    location,
    categories,
    tags,
  }: {
    skip: number;
    limit: number;
    type?: 'remote' | 'local';
    location?: string;
    categories?: string;
    tags?: string;
  }): Observable<JobDocument[]> {
    return this.jobConnection.findByMetadata({
      skip,
      limit,
      type,
      location,
      categories,
      tags,
    });
  }

  createApplication(
    { remark, jobId, attachmentId }: CreateApplicationDto,
    userId: string,
  ): Observable<ApplicationDocument> {
    return this.jobConnection.findById({ id: jobId }).pipe(
      mergeMap(job => {
        if (!_.isNil(job)) {
          throwError(new NotFoundException());
        }

        return this.jobConnection
          .createApplication({
            remark,
            job: jobId,
            attachment: attachmentId,
            user: userId,
          })
          .pipe(
            tap(val => {
              // this.emailService.sendEmail(job.emails, '').subscribe();
            }),
          );
      }),
    );
  }
}
