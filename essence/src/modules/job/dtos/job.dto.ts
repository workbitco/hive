import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import {
  IsArray,
  IsEnum,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';

export class CreateJobDto {
  @IsString()
  @ApiModelProperty()
  readonly position: string;

  @IsString()
  @IsEnum(['remote', 'local'])
  @ApiModelProperty({ enum: ['remote', 'local'] })
  readonly type: 'remote' | 'local';

  @IsString()
  @IsOptional()
  @ApiModelProperty()
  readonly location: string;

  @IsString()
  @ApiModelProperty()
  readonly category: string;

  @IsArray()
  @ApiModelProperty()
  readonly tags: string[];

  @IsString()
  @ApiModelProperty()
  readonly description: string;

  @IsString()
  @ApiModelProperty()
  readonly requirement: string;

  @Min(0)
  @IsNumber()
  @ApiModelProperty()
  readonly salary: number;

  @IsArray()
  @ApiModelProperty()
  readonly emails: string[];

  @ApiModelProperty()
  readonly stylings: string;

  @IsString()
  @ApiModelProperty()
  readonly companyId: string;
}

export class QueryJobDto {
  @IsNumberString()
  @ApiModelProperty()
  readonly skip: string;

  @IsNumberString()
  @ApiModelProperty()
  readonly limit: string;

  @IsEnum(['remote', 'local'])
  @IsOptional()
  @ApiModelPropertyOptional({ enum: ['remote', 'local'] })
  readonly type?: 'remote' | 'local';

  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional({ type: 'string' })
  readonly location?: string;

  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional({ isArray: true, type: 'string' })
  readonly categories?: string;

  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional({ isArray: true, type: 'string' })
  readonly tags?: string;
}
