import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateApplicationDto {
  @IsString()
  @ApiModelPropertyOptional()
  readonly remark: string;

  @IsString()
  @ApiModelProperty()
  readonly jobId: string;

  @IsString()
  @ApiModelProperty()
  readonly attachmentId: string;
}
