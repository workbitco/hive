import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from 'modules/auth/auth.module';
import { JobConnection } from 'modules/job/job.connection';
import { JobController } from 'modules/job/job.controller';
import { JobService } from 'modules/job/job.service';
import { MongooseFeatures } from 'schemas';

@Module({
  imports: [
    AuthModule,
    MongooseModule.forFeature([
      MongooseFeatures.application,
      MongooseFeatures.job,
    ]),
  ],
  controllers: [JobController],
  providers: [JobConnection, JobService],
})
export class JobModule {}
