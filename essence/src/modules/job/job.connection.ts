import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, of, Observable } from 'rxjs';
import { ApplicationDocument } from 'schemas/application/application.document';
import { JobDocument } from 'schemas/job/job.document';

@Injectable()
export class JobConnection {
  @InjectModel('Application')
  private readonly applicationPvd: Model<ApplicationDocument>;

  @InjectModel('Job')
  private readonly jobPvd: Model<JobDocument>;

  create({
    position,
    type,
    location,
    category,
    tags,
    description,
    requirement,
    salary,
    emails,
    stylings,
    company,
    user,
  }: Partial<JobDocument>): Observable<JobDocument> {
    return from(
      this.jobPvd.create({
        position,
        type,
        location,
        category,
        tags,
        description,
        requirement,
        salary,
        emails,
        stylings,
        company,
        user,
      }),
    );
  }

  findById({ id }: { id: string }): Observable<JobDocument> {
    return from(this.jobPvd.findById(id));
  }

  findByMetadata({
    skip,
    limit,
    type,
    location,
    categories,
    tags,
  }: {
    skip: number;
    limit: number;
    type?: 'remote' | 'local';
    location?: string;
    categories?: string;
    tags?: string;
  }): Observable<JobDocument[]> {
    this.jobPvd.aggregate([
      {
        $match: {
          $and: [
            { type },
            { categories: { $in: categories } },
            { tag: { $in: tags } },
            { $text: { $seach: location } },
          ],
        },
      },
      { $sort: { location: { $meta: 'location' } } },
      { $limit: limit },
      { $skip: skip },
    ]);
    return of([]);
  }

  createApplication({
    remark,
    job,
    attachment,
    user,
  }: Partial<ApplicationDocument>): Observable<ApplicationDocument> {
    return from(
      this.applicationPvd.create({
        remark,
        job,
        attachment,
        user,
      }),
    );
  }
}
