import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from 'guards/jwt-auth.guard';
import * as _ from 'lodash';
import { HashIdService } from 'modules/common/hashid.service';
import { CreateApplicationDto } from 'modules/job/dtos/application.dto';
import { CreateJobDto, QueryJobDto } from 'modules/job/dtos/job.dto';
import { JobService } from 'modules/job/job.service';
import { HashIdPipe } from 'pipes/hashid.pipe';
import { of, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { JobDocument } from 'schemas/job/job.document';

@Controller('job')
export class JobController {
  @Inject()
  private readonly jobService: JobService;

  @Inject()
  private readonly hashIdService: HashIdService;

  @ApiBearerAuth()
  @UsePipes(new HashIdPipe(['companyId']))
  @UseGuards(JwtAuthGuard)
  @Post('create')
  create(
    @Body() createJobDto: CreateJobDto,
    @Req() req: Request,
  ): Observable<Partial<JobDocument>> {
    const userId = _.get(req, 'user._id');
    return this.jobService
      .create(createJobDto, userId)
      .pipe(map(val => this.hashJobDocument(val)));
  }

  @UsePipes(new HashIdPipe(['id']))
  @Get(':id')
  findById(@Param('id') id: string): Observable<Partial<JobDocument>> {
    return this.jobService
      .findById(id)
      .pipe(map(val => this.hashJobDocument(val)));
  }

  @Get()
  findByMetadata(
    @Query() query: QueryJobDto,
  ): Observable<Partial<JobDocument>[]> {
    return this.jobService.findByMetadata({
      ...query,
      skip: Number(query.skip),
      limit: Number(query.limit),
    });
  }

  @ApiBearerAuth()
  @UsePipes(new HashIdPipe(['jobId', 'attachmentId']))
  @UseGuards(JwtAuthGuard)
  @Post('submit')
  createApplication(
    @Body() createApplicationDto: CreateApplicationDto,
    @Req() req: Request,
  ): Observable<{ success: boolean }> {
    const userId = _.get(req, 'user._id');
    return this.jobService
      .createApplication(createApplicationDto, userId)
      .pipe(
        map(val => ({
          success: true,
        })),
      )
      .pipe(
        catchError(error => {
          return of({ success: false });
        }),
      );
  }

  private hashJobDocument(jobDocument: JobDocument): {} {
    const company = this.hashIdService.convertDocumentToId(jobDocument.company);
    const user = this.hashIdService.convertDocumentToId(jobDocument.user);

    return {
      ...jobDocument.toJSON(),
      company,
      user,
      id: jobDocument.sid,
    };
  }
}
