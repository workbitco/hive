import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import * as _ from 'lodash';
import { HashIdService } from 'modules/common/hashid.service';
import { Model } from 'mongoose';
import { UserDocument } from 'schemas/user/user.document';

@Injectable()
export class AuthService {
  @Inject()
  private readonly hashIdService: HashIdService;

  @Inject()
  private readonly jwtService: JwtService;

  @InjectModel('User')
  private readonly userPvd: Model<UserDocument>;

  async validateUser(payload: {}): Promise<{}> {
    const sid = _.get(payload, 'id');
    if (_.isNil(sid)) return Promise.resolve(undefined);
    const user = await this.userPvd.findById(this.hashIdService.decode(sid));
    return user.toJSON();
  }

  sign(payload: {}, expiresIn: number = 1_296_000 /* 15 days */): string {
    return this.jwtService.sign(payload, {
      expiresIn,
      issuer: 'namland',
      subject: 'user',
    });
  }
}
