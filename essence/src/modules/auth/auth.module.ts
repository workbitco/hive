import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from 'modules/auth/auth.service';
import { JwtStrategy } from 'modules/auth/jwt.strategy';
import { MongooseFeatures } from 'schemas';

@Module({
  imports: [
    JwtModule.register({ secret: 'zxcdsas' }),
    MongooseModule.forFeature([MongooseFeatures.user]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],
  exports: [AuthService, PassportModule],
  providers: [AuthService, JwtStrategy],
})
export class AuthModule {}
