import { Module } from '@nestjs/common';
import { AuthModule } from 'modules/auth/auth.module';
import { CommonModule } from 'modules/common/common.module';
import { CompanyModule } from 'modules/company/company.module';
import { DatabaseModule } from 'modules/database.module';
import { JobModule } from 'modules/job/job.module';
import { StorageModule } from 'modules/storage/storage.module';
import { UserModule } from 'modules/user/user.module';

@Module({
  imports: [
    AuthModule,
    CommonModule,
    CompanyModule,
    DatabaseModule,
    JobModule,
    StorageModule,
    UserModule,
  ],
  exports: [AuthModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
