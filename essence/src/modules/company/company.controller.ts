import {
  Body,
  Controller,
  Get,
  Inject,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from 'guards/jwt-auth.guard';
import * as _ from 'lodash';
import { HashIdService } from 'modules/common/hashid.service';
import { CompanyService } from 'modules/company/company.service';
import {
  CreateCompanyDto,
  QueryCompanyDto,
} from 'modules/company/dtos/company.dto';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CompanyDocument } from 'schemas/company/company.document';

@Controller('company')
export class CompanyController {
  @Inject()
  private readonly companyService: CompanyService;

  @Inject()
  private readonly hashIdService: HashIdService;

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('create')
  create(
    @Body() createCompanyDto: CreateCompanyDto,
    @Req() req: Request,
  ): Observable<Partial<CompanyDocument>> {
    const userId = _.get(req, 'user._id');
    return this.companyService
      .create(
        {
          ...createCompanyDto,
          attachments: this.hashIdService.decodeArray(
            createCompanyDto.attachments,
          ),
        },
        userId,
      )
      .pipe(map(val => this.hashCompanyDocument(val)));
  }

  @Get()
  findByName(
    @Query() query: QueryCompanyDto,
  ): Observable<Partial<CompanyDocument>[]> {
    return this.companyService.findByName(query).pipe(
      map(val => {
        return val.map(el => {
          return {
            ...el.toJSON(),
            attachments: this.hashIdService.encodeArray(
              el.attachments as string[],
            ),
            id: el.sid,
          };
        });
      }),
    );
  }

  private hashCompanyDocument(companyDocument: CompanyDocument): {} {
    const attachments = (companyDocument.attachments as any[]).map(
      attachment => {
        return this.hashIdService.convertDocumentToId(attachment);
      },
    );
    const user = this.hashIdService.convertDocumentToId(companyDocument.user);

    return {
      ...companyDocument.toJSON(),
      attachments,
      user,
      id: companyDocument.sid,
    };
  }
}
