import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { CompanyDocument } from 'schemas/company/company.document';

@Injectable()
export class CompanyConnection {
  @InjectModel('Company')
  private readonly companyPvd: Model<CompanyDocument>;

  create({
    name,
    attachments,
    url,
    user,
  }: Partial<CompanyDocument>): Observable<CompanyDocument> {
    return from(this.companyPvd.create({ name, attachments, url, user }));
  }

  findByName({ name }: { name: string }): Observable<CompanyDocument[]> {
    return from(
      this.companyPvd.find({ name: { $regex: name, $options: 'i' } }),
    );
  }
}
