import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from 'modules/auth/auth.module';
import { CompanyConnection } from 'modules/company/company.connection';
import { CompanyController } from 'modules/company/company.controller';
import { CompanyService } from 'modules/company/company.service';
import { MongooseFeatures } from 'schemas';

@Module({
  imports: [AuthModule, MongooseModule.forFeature([MongooseFeatures.company])],
  controllers: [CompanyController],
  providers: [CompanyConnection, CompanyService],
})
export class CompanyModule {}
