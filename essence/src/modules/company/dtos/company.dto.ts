import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsString, ValidateNested } from 'class-validator';

export class CreateCompanyDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(type => String)
  @ApiModelProperty({ type: String, isArray: true })
  readonly attachments: string[];

  @IsString()
  @ApiModelProperty()
  readonly url: string;
}

export class QueryCompanyDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;
}
