import { Inject, Injectable } from '@nestjs/common';
import { CompanyConnection } from 'modules/company/company.connection';
import { CreateCompanyDto } from 'modules/company/dtos/company.dto';
import { Observable } from 'rxjs';
import { CompanyDocument } from 'schemas/company/company.document';

@Injectable()
export class CompanyService {
  @Inject()
  private readonly companyConnection: CompanyConnection;

  create(
    { name, attachments, url }: CreateCompanyDto,
    userId: string,
  ): Observable<CompanyDocument> {
    return this.companyConnection.create({
      name,
      attachments,
      url,
      user: userId,
    });
  }

  findByName({ name }: { name: string }): Observable<CompanyDocument[]> {
    return this.companyConnection.findByName({ name });
  }
}
