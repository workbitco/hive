import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as _ from 'lodash';
import { Model } from 'mongoose';
import { from, of, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CodeDocument } from 'schemas/code/code.document';
import { UserDocument } from 'schemas/user/user.document';

@Injectable()
export class UserConnection {
  @InjectModel('Code')
  private readonly codePvd: Model<CodeDocument>;

  @InjectModel('User')
  private readonly userPvd: Model<UserDocument>;

  create({ email, password }: Partial<UserDocument>): Observable<UserDocument> {
    return from(this.userPvd.create({ email, password }));
  }

  findByEmail({ email }: { email: string }): Observable<UserDocument> {
    return from(this.userPvd.findOne({ email }));
  }

  generateOTP({ email }): Observable<boolean> {
    return from(
      this.codePvd.create({
        email,
      }),
    ).pipe(map(() => true));
  }

  verifyOTP({ email, secret }: Partial<CodeDocument>): Observable<boolean> {
    return from(this.codePvd.findOne({ email, secret })).pipe(
      switchMap(val => {
        if (_.isNil(val)) {
          return of(false);
        }
        return of(true);
      }),
    );
  }
}
