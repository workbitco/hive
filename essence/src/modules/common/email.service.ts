import { Inject, Injectable } from '@nestjs/common';
import { SentMessageInfo, Transporter } from 'nodemailer';
import { from, Observable } from 'rxjs';
import { finalize, map, mergeMap, toArray } from 'rxjs/operators';

@Injectable()
export class EmailService {
  @Inject('NODEMAILER')
  private readonly emailClient: Transporter;

  sendEmail(emails: string[], template: string): Observable<number> {
    return from(emails)
      .pipe(
        mergeMap(email => {
          return from(this.emailClient.sendMail({
            from: 'WorkBit <nonereply@workbit.co>',
            to: email,
            subject: 'Node.js Email with Secure OAuth',
            html: `<div>
              <h1>Hello world</h1>
            </div>`,
          }) as Promise<SentMessageInfo>);
        }),
      )
      .pipe(toArray())
      .pipe(
        map(infos => {
          return infos.length;
        }),
      )
      .pipe(
        finalize(() => {
          this.emailClient.close();
        }),
      );
  }
}
