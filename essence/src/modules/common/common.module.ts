import { Global, HttpModule, Module } from '@nestjs/common';
import { EmailService } from 'modules/common/email.service';
import { HashIdService } from 'modules/common/hashid.service';
import { NodemailerProvider } from 'providers/email.provider';

@Global()
@Module({
  imports: [HttpModule],
  exports: [EmailService, HashIdService],
  providers: [EmailService, HashIdService, NodemailerProvider],
})
export class CommonModule {}
