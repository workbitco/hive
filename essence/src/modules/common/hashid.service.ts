import { Injectable } from '@nestjs/common';
import * as _ from 'lodash';
import { Types } from 'mongoose';
import { hashids } from 'schemas/schema.plugin';

@Injectable()
export class HashIdService {
  encode(str: string): string {
    return hashids.encodeHex(str);
  }

  decode(hash: string): string {
    return hashids.decodeHex(hash);
  }

  encodeArray(strs?: string[]): string[] | undefined {
    if (_.isNil(strs)) {
      return undefined;
    }

    return strs.map(str => hashids.encodeHex(str));
  }

  decodeArray(hashes?: string[]): string[] | undefined {
    if (_.isNil(hashes)) {
      return undefined;
    }

    return hashes.map(hash => hashids.decodeHex(hash));
  }

  convertDocumentToId = (document: any) => {
    if (Types.ObjectId.isValid(document)) {
      return hashids.encodeHex(document.toString());
    }

    if (typeof document === 'string') {
      return hashids.encodeHex(document);
    }

    const sid = _.get(document, 'sid');
    return _.isNil(sid) ? document : sid;
  }
}
