import { config } from 'config';
import * as nodemailer from 'nodemailer';

export const NodemailerProvider = {
  provide: 'NODEMAILER',
  useFactory: () => {
    const smtpTransport = nodemailer.createTransport({
      service: 'gmail',
      secure: false,
      port: 25,
      auth: {
        type: 'OAuth2',
        user: config.GMAIL_ACCOUNT,
        clientId: config.GAPI_CLIENT_ID,
        clientSecret: config.GAPI_CLIENT_SECRET,
        refreshToken: config.GAPI_REFRESH_TOKEN,
      },
      tls: {
        rejectUnauthorized: false,
      },
    });
    return smtpTransport;
  },
};
