import { config } from 'config';
import { Client } from 'minio';

export const MinIOProvider = {
  provide: 'MINIO',
  useFactory: () => {
    const minioClient = new Client({
      endPoint: config.S3_URL,
      region: config.S3_REGION,
      accessKey: config.S3_ACCESS_KEY,
      secretKey: config.S3_SECRET_KEY,
    });
    return minioClient;
  },
};
