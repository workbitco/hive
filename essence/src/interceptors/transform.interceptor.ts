import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export type Response<T> = Pick<any, number | symbol>;

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response<T>> {
  private static omitFields = ['_id', '__v', 'password'];

  private static transformObject(data: any) {
    if (_.isArray(data)) {
      return data.map(el => TransformInterceptor.transformObject(el));
    }

    if (_.isPlainObject(data)) {
      return Object.keys(data).reduce((aggr, key) => {
        if (TransformInterceptor.omitFields.includes(key)) {
          return aggr;
        }

        return {
          ...aggr,
          [key]: TransformInterceptor.transformObject(data[key]),
        };
      }, {});
    }

    return data;
  }

  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    return next
      .handle()
      .pipe(map(data => TransformInterceptor.transformObject(data)));
  }
}
