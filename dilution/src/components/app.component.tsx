import grapesjs from 'grapesjs';
import React from 'react';

export const App = () => {
  React.useLayoutEffect(() => {

    const editor = grapesjs.init({
      container: '#gjs',
      components: '<div class="txt-red">Hello world!</div>',
      style: '.txt-red{color: red}',
    });
  });

  return (
    <>
      <div id='gjs'></div>
    </>
  );
};
